#include "cnit.h"

#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    struct node *next;
    int (*f)();
    const char *msg;
} node;

static node origin = { NULL, NULL, NULL };

static node *last = &origin;
static int test_count = 0;

void cnit_add_test(int (*fptr)(), const char *msg) {
    node *next = (node *) malloc(sizeof(node));
    next->next = NULL;
    next->f = fptr;
    next->msg = msg;

    last->next = next;
    last = next;
    test_count++;
}

int cnit_run_tests() {
    node *curr_node = origin.next;
    int passed = 0;
    int curr_test = 1;
    printf("Running tests...\n");
    while (curr_node) {
        int line = curr_node->f();
        node *next_node = curr_node->next;
        if (line) {
            printf("%3d / %3d | " CONSOLE_RED "X" CONSOLE_RESET " | %s | line %d\n",
                   curr_test, test_count, curr_node->msg, line);
        } else {
            printf("%3d / %3d | " CONSOLE_GREEN "O" CONSOLE_RESET " | %s\n",
                   curr_test, test_count, curr_node->msg);
            passed++;
        }
        curr_test++;
        free(curr_node);
        curr_node = next_node;
    }
    printf("%d/%d tests passed. (%.0f%%)\n", passed, test_count, (double) passed / test_count * 100);
    return passed != test_count;
}

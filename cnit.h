#ifndef CNIT_CNIT_H
#define CNIT_CNIT_H

#define CNIT_ASSERT(x) do { if (!(x)) { return __LINE__; } } while (0)

#ifdef DISABLE_COLORS
    #define CONSOLE_GREEN ""
    #define CONSOLE_RED ""
    #define CONSOLE_RESET ""
#else
    #define CONSOLE_GREEN "\033[0;32m"
    #define CONSOLE_RED "\033[0;31m"
    #define CONSOLE_RESET "\033[0m"
#endif

/**
 * Add a new test to be run. The test function must take zero arguments,
 * and return an integer. To signify that the test has failed, return __LINE__.
 * Return zero at the end of the function to signal that it has succeeded.
 * @param fptr The pointer to the function.
 * @param msg A message that represents the test.
 */
void cnit_add_test(int (*fptr)(), const char *msg);

/**
 * Runs all the added tests. Returns 0 if all tests passed, and non-zero if a test failed.
 * @return 0 if all tests passed, non-zero otherwise.
 */
int cnit_run_tests();

#endif
